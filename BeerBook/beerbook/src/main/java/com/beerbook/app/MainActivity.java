package com.beerbook.app;

import android.content.Context;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.beerbook.app.facade.BeerFacade;
import com.beerbook.app.fragment.BeerFilterFragment;
import com.beerbook.app.fragment.BeerLocationListFragment;
import com.beerbook.app.fragment.BeerMapFragment;
import com.beerbook.app.fragment.SplashFragment;
import com.beerbook.app.rest.RestClient;
import com.beerbook.app.util.BeerUtil;
import com.google.android.gms.maps.model.CameraPosition;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity {
    private BeerFacade beerFacade;

    @InjectView(R.id.main_buttons) View buttonsLayout;
    @InjectViews({R.id.beer_map_button, R.id.beer_browser_button, R.id.beer_list_button}) View[] buttons;
    @InjectView(R.id.beer_map_button) ImageView beerMapButton;
    @InjectView(R.id.beer_browser_button) ImageView beerBrowserButton;
    @InjectView(R.id.beer_list_button) ImageView beerListButton;
    private int rootActivity = 0;
    public boolean isFirstLoad = true;
    public CameraPosition cameraPosition = null;
    private Typeface beerTypeface;
    private LocationListener locationListener = new LocationListener() { //we don't need to actually do anything with these
        @Override public void onLocationChanged(Location location) {

        }

        @Override public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override public void onProviderEnabled(String s) {

        }

        @Override public void onProviderDisabled(String s) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        onMapButtonClicked();
        BeerMapFragment beerMapFragment = new BeerMapFragment();
        beerMapFragment.setRetainInstance(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, beerMapFragment);
        ft.commit();
        //beerTypeface = Typeface.createFromAsset(getAssets(), "fonts/beerfont.ttf");
        beerTypeface = Typeface.DEFAULT;

        rootActivity = 0;
        beerMapButton.setBackgroundResource(R.drawable.selector_tab_button_pressed);
        beerMapButton.setImageResource(R.drawable.beer_map_pressed);


        BeerUtil.constructSets();
    }

    @Override protected void onStart() {
        super.onStart();
        DialogFragment splashScreen = new SplashFragment();
        splashScreen.show(getSupportFragmentManager(), "Splash");
        //LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //locationManager.requestSingleUpdate(locationManager.getBestProvider(new Criteria(), true), 100, 10, locationListener);
    }

    @Override protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(locationManager.getBestProvider(new Criteria(), true), 100, 10, locationListener);

    }

    @Override protected void onPause() {
        super.onPause();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);
    }

    public void showButtons() {
        buttonsLayout.setVisibility(View.VISIBLE);
    }

    public void viewingDetailsFromMap(){
        rootActivity = 3;
    }

    @OnClick(R.id.beer_map_button) public void onMapButtonClicked() {
        isFirstLoad = true;
        cameraPosition = null;
        goToMap();

    }

    private void goToMap() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            fm.popBackStackImmediate();
        }

        rootActivity = 0;
        BeerMapFragment mapFragment = new BeerMapFragment();
        mapFragment.setRetainInstance(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, mapFragment);
        ft.commit();
        for (View button :buttons) {
            button.setBackgroundResource(R.drawable.selector_tab_button);
        }
        beerMapButton.setBackgroundResource(R.drawable.selector_tab_button_pressed);
        beerBrowserButton.setImageResource(R.drawable.selector_browser_button);
        beerMapButton.setImageResource(R.drawable.beer_map_pressed);
        beerListButton.setImageResource(R.drawable.selector_list_button);
    }


    @OnClick(R.id.beer_browser_button) public void onBrowserButtonClicked() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            fm.popBackStackImmediate();
        }

        rootActivity = 1;
        BeerFilterFragment filterFragment = new BeerFilterFragment();
        filterFragment.setRetainInstance(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, filterFragment);
        ft.commit();
        for (View button :buttons) {
            button.setBackgroundResource(R.drawable.selector_tab_button);
        }
        beerBrowserButton.setBackgroundResource(R.drawable.selector_tab_button_pressed);
        beerBrowserButton.setImageResource(R.drawable.beer_browser_pressed);
        beerMapButton.setImageResource(R.drawable.selector_map_button);
        beerListButton.setImageResource(R.drawable.selector_list_button);


    }

    @OnClick(R.id.beer_list_button) void onListButtonClicked() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for(int i = 0; i < count; ++i) {
            fm.popBackStackImmediate();
        }

        rootActivity = 2;
        BeerLocationListFragment beerLocationListFragment = new BeerLocationListFragment();
        beerLocationListFragment.setRetainInstance(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, beerLocationListFragment);
        ft.commit();
        for (View button :buttons) {
            button.setBackgroundResource(R.drawable.selector_tab_button);
        }
        beerListButton.setBackgroundResource(R.drawable.selector_tab_button_pressed);
        beerBrowserButton.setImageResource(R.drawable.selector_browser_button);
        beerMapButton.setImageResource(R.drawable.selector_map_button);
        beerListButton.setImageResource(R.drawable.beer_list_pressed);
    }

    public BeerFacade  getBeerFacade() {
        if (beerFacade == null) {
            RestClient restClient = new RestClient();
            restClient.setup();
            beerFacade = new BeerFacade(restClient);
        }
        return beerFacade;
    }


    @Override public void onBackPressed() {
        if (rootActivity == 3) {
            goToMap();
            return;
        }
        super.onBackPressed();
    }

    public Typeface getBeerTypeface() {
        return beerTypeface;
    }

}
