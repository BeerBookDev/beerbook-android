package com.beerbook.app.facade;

import com.beerbook.app.model.BeerBrandList;
import com.beerbook.app.model.BeerList;
import com.beerbook.app.model.BeerTypeList;
import com.beerbook.app.model.CollectionWrapper;
import com.beerbook.app.model.Company;
import com.beerbook.app.model.StringList;
import com.beerbook.app.rest.Callback;
import com.beerbook.app.rest.RestClient;
import com.beerbook.app.util.Logger;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 8/23/15.
 */
public class BeerFacade {

  private RestClient restClient;

  public BeerFacade(RestClient client) {
    restClient = client;
  }

  public void getItems(String url, Class clazz, final Callback<CollectionWrapper> callback) {
    Logger.debug("Url =" + url);

    restClient.get(url, null, clazz, new Callback<CollectionWrapper>() {
      @Override public void onTaskCompleted(CollectionWrapper result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeers(final Callback<BeerList> callback) {
    restClient.get("beers?associated=true", null, BeerList.class, new Callback<BeerList>() {
      @Override public void onTaskCompleted(BeerList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeers(String beerUrl, final Callback<BeerList> callback) {
    restClient.get(beerUrl.substring(1), null, BeerList.class, new Callback<BeerList>() {
      @Override public void onTaskCompleted(BeerList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeerBrands(final Callback<BeerBrandList> callback) {
    restClient.get("brands?associated=true", null, BeerBrandList.class, new Callback<BeerBrandList>() {
      @Override public void onTaskCompleted(BeerBrandList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeerBrands(String url, final Callback<BeerBrandList> callback) {
    restClient.get(url.substring(1), null, BeerBrandList.class, new Callback<BeerBrandList>() {
      @Override public void onTaskCompleted(BeerBrandList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeerTypes(final Callback<BeerTypeList> callback) {
    restClient.get("beertypes?associated=true", null, BeerTypeList.class, new Callback<BeerTypeList>() {
      @Override public void onTaskCompleted(BeerTypeList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getBeerTypes(String url, final Callback<BeerTypeList> callback) {
    restClient.get(url.substring(1), null, BeerTypeList.class, new Callback<BeerTypeList>() {
      @Override public void onTaskCompleted(BeerTypeList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getList(String listType, final Callback<StringList> callback) {
    restClient.get(listType, null, StringList.class, new Callback<StringList>() {
      @Override public void onTaskCompleted(StringList result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getCompanyDetails(String url, final Callback<Company> callback) {
    restClient.get(url, null, Company.class, new Callback<Company>() {
      @Override public void onTaskCompleted(Company result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }

  public void getCompanyDetails(Company company, final Callback<Company> callback) {
    restClient.get("companies/176062/?expand=beers", null, ArrayList.class, new Callback<Company>() {
      @Override public void onTaskCompleted(Company result) {
        callback.onTaskCompleted(result);
      }

      @Override public void onTaskFailure(String Reason) {
        callback.onTaskFailure(Reason != null ? Reason : "Unknown Error");
      }
    });
  }
}
