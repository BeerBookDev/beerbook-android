package com.beerbook.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.beerbook.app.R;
import com.beerbook.app.util.BeerUtil;

/**
 * Created by cameron.lewis on 5/27/15.
 */
public class BeerFilterFragment extends Fragment {

  @OnClick(R.id.brand_filter_button) void onBrandClicked() {
    displayList(BeerFinderFragment.BRAND);
  }

  @OnClick(R.id.name_filter_button) void onNameClicked() {
    displayList(BeerFinderFragment.BEER);
  }

  @OnClick(R.id.type_filter_button) void onTypeClicked() {
    displayList(BeerFinderFragment.TYPE);
  }

  @OnClick(R.id.abv_filter_button) void onABVClicked() {
    displayList(BeerFinderFragment.ABV);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.beer_filter_fragment, container, false);
    ButterKnife.inject(this, root);

    return root;

  }


  public void displayList(int type) {
    BeerFinderFragment beerFinderFragment = new BeerFinderFragment();
    beerFinderFragment.setListType(type);
    beerFinderFragment.setRetainInstance(true);
    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.main_fragment_container, beerFinderFragment);
    ft.addToBackStack("meh" + type);
    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    ft.commit();
  }
}
