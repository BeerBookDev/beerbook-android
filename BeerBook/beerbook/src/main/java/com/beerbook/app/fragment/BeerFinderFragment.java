package com.beerbook.app.fragment;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.beerbook.app.MainActivity;
import com.beerbook.app.R;
import com.beerbook.app.facade.BeerFacade;
import com.beerbook.app.model.Beer;
import com.beerbook.app.model.BeerABV;
import com.beerbook.app.model.BeerABVList;
import com.beerbook.app.model.BeerBrand;
import com.beerbook.app.model.BeerBrandList;
import com.beerbook.app.model.BeerList;
import com.beerbook.app.model.BeerType;
import com.beerbook.app.model.BeerTypeList;
import com.beerbook.app.model.CollectionWrapper;
import com.beerbook.app.model.DataObject;
import com.beerbook.app.model.StringList;
import com.beerbook.app.rest.Callback;
import com.beerbook.app.util.BeerNameUtil;
import com.beerbook.app.util.BeerStyleUtil;
import com.beerbook.app.util.InfiniteListView;
import com.beerbook.app.util.Logger;
import com.beerbook.app.util.OnScrollListAdapter;
import java.util.ArrayList;

/**
 * Created by cameron.lewis on 11/27/14.
 */
public class BeerFinderFragment extends Fragment implements OnScrollListAdapter.OnScrollListener {

  public static int BEER = 0;
  public static int TYPE = 1;
  public static int BRAND = 2;
  public static int ABV = 3;


  @InjectView(R.id.beer_finder_list_view) InfiniteListView beerListView;
  @InjectView(R.id.beer_finder_filter_text) EditText searchEditText;

  private ArrayList<String> listItems;
  private ArrayList<DataObject> dataObjects;
  private BeerListAdapter adapter;
  private int listType;
  private String href;
  private String nextSetUrl;
  private String searchText;
  private String priorSearch;



  @OnClick(R.id.beer_finder_search_button) void onSearchClicked() {
    searchText = searchEditText.getText().toString();
    nextSetUrl = null;
    listItems = new ArrayList<>();
    dataObjects = new ArrayList<>();
    adapter.notifyDataSetChanged();
    fetchItems();
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    listItems = new ArrayList<>();
    dataObjects = new ArrayList<>();
    fetchItems();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.fragment_beer_finder, container, false);
    ButterKnife.inject(this, root);

    searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        onSearchClicked();
        return false;
      }
    });

    adapter = new BeerListAdapter(getActivity(), R.layout.list_item_view, this);
    beerListView.setAdapter(adapter);
    beerListView.setIsBottomOfList(nextSetUrl == null || nextSetUrl.length() == 0);

    beerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (listType == BEER) {
          BeerLocationListFragment beerLocationListFragment = new BeerLocationListFragment();
          beerLocationListFragment.setRetainInstance(true);
          beerLocationListFragment.setBeerId(dataObjects.get(i).getHref());
          FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
          ft.replace(R.id.main_fragment_container, beerLocationListFragment);
          ft.addToBackStack("mehhe");
          ft.commit();
        } else {
          BeerFinderFragment beerFinderFragment = new BeerFinderFragment();
          beerFinderFragment.setRetainInstance(true);
          beerFinderFragment.listType = BEER;
          beerFinderFragment.href = dataObjects.get(i).getHref();
          FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
          ft.replace(R.id.main_fragment_container, beerFinderFragment);
          ft.addToBackStack("megsdsgh");
          ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
          ft.commit();
        }
      }
    });



    return root;
  }


  private void fetchItems() {
    BeerFacade facade = ((MainActivity) getActivity()).getBeerFacade();
    String url;
    Class clazz;
    if (listType == BEER) {
      url = "beer";
      clazz = BeerList.class;
    } else if (listType == TYPE) {
      url = "beertypes";
      clazz = BeerTypeList.class;
    } else if (listType == BRAND){
      url = "brands";
      clazz = BeerBrandList.class;
    } else {
      url = "abvs";
      clazz = BeerABVList.class;
    }

    if (nextSetUrl != null && !nextSetUrl.isEmpty()) {
      url = nextSetUrl.substring(1);
    } else if (href != null && !href.isEmpty()) {
      url = href.substring(1) + "/beers";
      if (href.contains("abv")) {
        url = "beers?abv=" + href.replace("/abvs/", "");
      }
    }

    if (nextSetUrl == null && searchText != null && !searchText.isEmpty()) {
      url += url.contains("?") ? "&" : "?";
      url += "searchstring=" + searchText;
      if (priorSearch != null && priorSearch != searchText) {
        listItems = new ArrayList<>();
        dataObjects = new ArrayList<>();
      }
    }
    priorSearch = searchText;


    if (nextSetUrl == null || nextSetUrl.isEmpty()) {
      LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
      Criteria criteria = new Criteria();

      Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
      if (location != null) {
        url +=  url.contains("?") ? "&" : "?";
        url += "latitude=" + location.getLatitude();
        url += "&longitude=" + location.getLongitude();
        url += "&radius=" + (url.contains("searchstring") ? 50 : 5);
      } else {
        Toast.makeText(getActivity(), "Could not find your location!", Toast.LENGTH_SHORT).show();
      }
    }


    facade.getItems(url, clazz, new Callback<CollectionWrapper>() {
      @Override public void onTaskCompleted(CollectionWrapper result) {
        processResponse(result);
      }

      @Override public void onTaskFailure(String Reason) {

      }
    });
  }

  private void processResponse(CollectionWrapper result) {
    if (result instanceof BeerList) {
      for (Beer beer : ((BeerList) result).getItems()) {
        listItems.add(beer.getDisplayName());
        dataObjects.add(beer);
      }
    } else if (result instanceof BeerTypeList) {
      for (BeerType beerType : ((BeerTypeList) result).getItems()) {
        listItems.add(beerType.getType());
        dataObjects.add(beerType);
      }
    } else if (result instanceof BeerBrandList) {
      for (BeerBrand beerBrand : ((BeerBrandList) result).getItems()) {
        listItems.add(beerBrand.getBrand());
        dataObjects.add(beerBrand);
      }
    } else {
      for (BeerABV beerBrand : ((BeerABVList) result).getItems()) {
        listItems.add(beerBrand.getDisplayName());
        dataObjects.add(beerBrand);
      }
    }

    adapter.notifyDataSetChanged();
    nextSetUrl = result.getNext();
    beerListView.setIsBottomOfList(nextSetUrl == null || nextSetUrl.length() == 0);
  }


  public void setListType(int listType) {
    this.listType = listType;
  }

  @Override public void onListScroll(int position, final Callback<Object> callback) {
    callback.onTaskCompleted(null);
    if (!beerListView.getIsBottomOfList()) {
      fetchItems();
    }

    //
    //BeerFacade facade = ((MainActivity) getActivity()).getBeerFacade();
    //if (listType == BEER) {
    //  facade.getBeers(nextSetUrl, new Callback<BeerList>() {
    //    @Override public void onTaskCompleted(BeerList result) {
    //      processBeerResponse(result);
    //      callback.onTaskCompleted(result);
    //    }
    //
    //    @Override public void onTaskFailure(String Reason) {
    //      callback.onTaskFailure(Reason);
    //    }
    //  });
    //} else if (listType == BRAND) {
    //  facade.getBeerBrands(nextSetUrl, new Callback<BeerBrandList>() {
    //    @Override public void onTaskCompleted(BeerBrandList result) {
    //      for (BeerBrand item: result.getItems()) {
    //        listItems.add(item.getBrand());
    //        dataObjects.add(item);
    //      }
    //      nextSetUrl = result.getNext();
    //      adapter.notifyDataSetChanged();
    //      callback.onTaskCompleted(result);
    //    }
    //
    //    @Override public void onTaskFailure(String Reason) {
    //      callback.onTaskFailure(Reason);
    //    }
    //  });
    //} else if (listType == TYPE) {
    //  facade.getBeerTypes(nextSetUrl, new Callback<BeerTypeList>() {
    //    @Override public void onTaskCompleted(BeerTypeList result) {
    //      for (BeerType beerType : result.getItems()) {
    //        listItems.add(beerType.getType());
    //        dataObjects.add(beerType);
    //      }
    //      nextSetUrl = result.getNext();
    //      adapter.notifyDataSetChanged();
    //      callback.onTaskCompleted(result);
    //    }
    //
    //    @Override public void onTaskFailure(String Reason) {
    //      callback.onTaskFailure(Reason);
    //    }
    //  });
    //}


  }

  private class BeerListAdapter extends OnScrollListAdapter<String> {
    LayoutInflater mInflater;


    public BeerListAdapter(Context context, int resource, OnScrollListener listener) {
      super(context, resource, listItems, listener);
      mInflater = LayoutInflater.from((context));
    }

    @Override public int getCount() {
      return listItems.size();
    }

    @Override public View getView(int i, View view, ViewGroup viewGroup) {
      if (view == null) {
        view = mInflater.inflate(R.layout.list_item_view, viewGroup, false);
      }
      TextView nameView = ButterKnife.findById(view, R.id.list_item_text);
      nameView.setText(listItems.get(i));
      nameView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());

      if (listType == BEER) {
        TextView brandView = ButterKnife.findById(view, R.id.list_item_secondary_text);
        brandView.setText(((Beer) dataObjects.get(i)).getBrandName() + " - " + ((Beer) dataObjects.get(i)).getAbv() + "%");
        brandView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());
        brandView.setVisibility(View.VISIBLE);
      }
      checkScroll(i);

      return view;
    }
  }
}
