package com.beerbook.app.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import com.beerbook.app.MainActivity;
import com.beerbook.app.R;
import com.beerbook.app.facade.BeerFacade;
import com.beerbook.app.model.Beer;
import com.beerbook.app.model.BeerBrandList;
import com.beerbook.app.model.BeerList;
import com.beerbook.app.model.BeerLocation;
import com.beerbook.app.model.BeerLocationList;
import com.beerbook.app.model.BeerTypeList;
import com.beerbook.app.model.CollectionWrapper;
import com.beerbook.app.model.Company;
import com.beerbook.app.model.Special;
import com.beerbook.app.model.SpecialList;
import com.beerbook.app.rest.Callback;
import com.beerbook.app.util.BeerNameUtil;
import com.beerbook.app.util.BeerStyleUtil;
import com.beerbook.app.util.InfiniteListView;
import com.beerbook.app.util.OnScrollListAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by cameron.lewis on 6/29/15.
 */
public class BeerLocationDetailsFragment extends Fragment implements OnScrollListAdapter.OnScrollListener {

  @InjectView(R.id.location_details_name) TextView nameView;
  @InjectView(R.id.location_details_item_num_stars_image_view) View numStarsImageView;
  @InjectView(R.id.location_details_beer_list_view) InfiniteListView beerListView;
  @InjectView(R.id.location_beer_menu_button_toggle) Button beerSpecialToggle;

  public BeerLocation beerLocation;
  private ArrayList<Beer> beers = new ArrayList<>();
  private ArrayList<Special> specials = new ArrayList<>();
  private BeerListAdapter adapter;
  private String nextSetUrl;
  private boolean isShowingSpecials = false;


  @OnClick(R.id.location_details_item_num_stars_image_view) void onRatingClicked() {
    onAdminWebsiteClicked();
  }


    @OnClick(R.id.location_details_admin_website_button) void onAdminWebsiteClicked() {
    if (beerLocation.getAdminUrl() == null) {
      new AlertDialog.Builder(getActivity())
          .setMessage("There is no administrator url associated with this location.").show();
      return;
    }
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(beerLocation.getAdminUrl()));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getActivity().startActivity(intent);
  }

  @OnClick(R.id.location_details_website_button) void onWebsiteClick() {
    if (beerLocation.getWebsite() == null) {
      new AlertDialog.Builder(getActivity())
          .setMessage("There is no website associated with this location.").show();
      return;
    }
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(beerLocation.getWebsite()));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getActivity().startActivity(intent);
  }

  @OnClick(R.id.location_details_pin_button) void onMapButtonClicked() {
    //BeerMapFragment mapFragment = new BeerMapFragment();
    //mapFragment.setStoreLocation(new LatLng(beerLocation.getLatitude(), beerLocation.getLongitude()));
    //FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
    //ft.replace(R.id.main_fragment_container, mapFragment);
    //ft.addToBackStack("fdeeeefk");
    //ft.commit();


    //Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345"));
    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
        beerLocation.getLatitude(), beerLocation.getLongitude(), beerLocation.getDisplayName() );
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    getActivity().startActivity(intent);
  }

  @OnClick(R.id.location_beer_menu_button_toggle) void onToggleClick() {
    nextSetUrl = null;
    if (isShowingSpecials) {
      beers = new ArrayList<>();
      fetchItems();
      beerSpecialToggle.setText("Specials");
    } else {
      specials = new ArrayList<>();
      fetchSpecials();
      beerSpecialToggle.setText("Beers");
    }
    isShowingSpecials = !isShowingSpecials;
    adapter = new BeerListAdapter(getActivity(), R.layout.list_item_view, this);
    beerListView.setAdapter(adapter);
  }

  @OnItemClick(R.id.location_details_beer_list_view) void onSpecialClicked(int position) {
    if (isShowingSpecials) {
      AlertDialog dialog = new AlertDialog.Builder(getActivity())
          .setMessage(specials.get(position).getSpecialText())
          .setCancelable(true)
          .show();
      dialog.setCanceledOnTouchOutside(true);

    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_beer_location_details, container, false);
    ButterKnife.inject(this, root);

    ImageView locationImageView = ButterKnife.findById(root, R.id.location_details_image);
    switch (beerLocation.getLicenseType()) {
      case CONSUME:
        locationImageView.setImageResource(R.drawable.location_type_restaurant);
        break;
      case RETAIL:
        locationImageView.setImageResource(R.drawable.location_type_grocery);
        break;
      case BREWERY:
        locationImageView.setImageResource(R.drawable.location_type_brewery);
        break;
      case GAS_STATION:
        locationImageView.setImageResource(R.drawable.location_type_gas_station);
        break;
      case UNKNOWN:
        locationImageView.setImageResource(R.drawable.location_type_unknown);
        break;
    }

    beerSpecialToggle.setTypeface(((MainActivity) getActivity()).getBeerTypeface());
    if (beerLocation.getSpecials().getItems().size() > 0) {
      beerSpecialToggle.setVisibility(View.VISIBLE);
    }

    nameView.setText(beerLocation.getDisplayName());
    nameView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());

    int rating = beerLocation.getRating();

    if (rating == 0) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_0);
    } else if (rating == 1) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_1);
    } else if (rating == 2) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_2);
    } else if (rating == 3) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_3);
    } else if (rating == 4) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_4);
    } else if (rating == 5) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_5);
    } else if (rating == 6) {
      numStarsImageView.setBackgroundResource(R.drawable.rating_6);
    }

    adapter = new BeerListAdapter(getActivity(), R.layout.list_item_view, this);
    beerListView.setAdapter(adapter);

    fetchItems();
    return root;
  }


  private void fetchItems() {

    String url = beerLocation.getHref() + "/beers" ;

    if (nextSetUrl != null && !nextSetUrl.isEmpty()) {
      url = nextSetUrl;
    }
    ((MainActivity) getActivity()).getBeerFacade().getBeers(url, new Callback<BeerList>() {
      @Override public void onTaskCompleted(BeerList result) {
        beers.addAll(result.getItems());
        nextSetUrl = result.getNext();
        beerListView.setIsBottomOfList(nextSetUrl == null);

        adapter.notifyDataSetChanged();
      }

      @Override public void onTaskFailure(String Reason) {

      }
    });
  }

  private void fetchSpecials() {

    String url = beerLocation.getHref().substring(1) + "/specials" + "?expand=specials";

    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    Criteria criteria = new Criteria();
    final Location myLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
    if (myLocation != null) {
      //url += "&latitude=" + myLocation.getLatitude();
      //url += "&longitude=" + myLocation.getLongitude();
      //url += "&radius=5";

      if (nextSetUrl != null && !nextSetUrl.isEmpty()) {
        url = nextSetUrl;
      }
      ((MainActivity) getActivity()).getBeerFacade().getItems(url, SpecialList.class, new Callback<CollectionWrapper>() {
        @Override public void onTaskCompleted(CollectionWrapper result) {
          specials.addAll(((SpecialList) result).getItems());
          nextSetUrl = result.getNext();
          beerListView.setIsBottomOfList(nextSetUrl == null);

          adapter.notifyDataSetChanged();
        }

        @Override public void onTaskFailure(String Reason) {

        }
      });
    } else {
      Toast.makeText(getActivity(), "Could not find your location!", Toast.LENGTH_SHORT).show();
    }
  }

  @Override public void onListScroll(int position, Callback<Object> callback) {
    callback.onTaskCompleted(null);
    if (!beerListView.getIsBottomOfList()) {
      fetchItems();
    }
  }

  private class BeerListAdapter extends OnScrollListAdapter<Beer> {
    LayoutInflater mInflater;


    public BeerListAdapter(Context context, int resource, OnScrollListener listener) {
      super(context, resource, beers, listener);
      mInflater = LayoutInflater.from((context));
    }

    @Override public int getCount() {
      if (isShowingSpecials) {
        return specials.size();
      } else {
        return beers.size();
      }
    }

    @Override public View getView(int i, View view, ViewGroup viewGroup) {
      if (view == null) {
        view = mInflater.inflate(R.layout.list_item_view, viewGroup, false);
      }

      TextView nameView = ButterKnife.findById(view, R.id.list_item_text);
      nameView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());

      TextView brandView = ButterKnife.findById(view, R.id.list_item_secondary_text);
      brandView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());
      brandView.setVisibility(View.VISIBLE);
      if (isShowingSpecials) {
        nameView.setText(specials.get(i).getTitle());
        brandView.setText(specials.get(i).getSpecialText());
      } else {
        nameView.setText(beers.get(i).getDisplayName());
        brandView.setText(beers.get(i).getBrandName());
      }


      checkScroll(i);

      return view;
    }
  }
}
