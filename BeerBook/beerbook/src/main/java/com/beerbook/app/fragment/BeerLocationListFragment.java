package com.beerbook.app.fragment;

import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.beerbook.app.MainActivity;
import com.beerbook.app.R;
import com.beerbook.app.facade.BeerFacade;
import com.beerbook.app.model.BeerBrandList;
import com.beerbook.app.model.BeerList;
import com.beerbook.app.model.BeerLocation;
import com.beerbook.app.model.BeerLocationList;
import com.beerbook.app.model.BeerTypeList;
import com.beerbook.app.model.CollectionWrapper;
import com.beerbook.app.rest.Callback;
import com.beerbook.app.util.InfiniteListView;
import com.beerbook.app.util.OnScrollListAdapter;
import java.util.ArrayList;

/**
 * Created by cameron.lewis on 6/29/15.
 */
public class BeerLocationListFragment extends Fragment implements OnScrollListAdapter.OnScrollListener{


  @InjectView(R.id.beer_location_list_view) InfiniteListView locationListView;
  @InjectView(R.id.beer_location_list_empty_text) TextView emptyTextView;
  @InjectView(R.id.beer_location_list_filter_text) EditText searchEditText;

  private LocationListAdapter adapter;
  private ArrayList<BeerLocation> locationList = new ArrayList<>();
  private String beerId;
  private String nextUrl;

  private String searchText;
  private String priorSearch;


  @OnClick(R.id.beer_location_list_search_button) void onSearchClicked() {
    searchText = searchEditText.getText().toString();
    nextUrl = null;
    locationList = new ArrayList<>();
    adapter.notifyDataSetChanged();
    fetchItems();
  }



  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    fetchItems();

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.fragment_beer_location_list, container, false);
    ButterKnife.inject(this, root);

    locationListView.setIsBottomOfList(nextUrl == null);


    searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        onSearchClicked();
        return false;
      }
    });


    emptyTextView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());


    adapter = new LocationListAdapter(getActivity(), R.layout.location_list_item_view, this);

    locationListView.setAdapter(adapter);

    locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        BeerLocationDetailsFragment beerLocationDetailsFragment = new BeerLocationDetailsFragment();
        beerLocationDetailsFragment.beerLocation = locationList.get(i);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_fragment_container, beerLocationDetailsFragment);
        ft.addToBackStack("dsfsfmeh");
        ft.commit();
      }
    });

    return root;
  }


  private void fetchItems() {
    String url = "companies?latitude=";
    if (beerId != null) {
      url = beerId.substring(1) + "/companies?associated=true&latitude=";
    }
    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    Criteria criteria = new Criteria();

    Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
    if (location != null) {
      url += location.getLatitude();
      url += "&longitude=" + location.getLongitude();
      url += "&radius=5";
    } else {
      Toast.makeText(getActivity(), "Could not find your location!", Toast.LENGTH_SHORT).show();
    }


    if (nextUrl != null) {
      url = nextUrl.substring(1);
    }


    if (searchText != null && !searchText.isEmpty()) {
      url += "&searchstring=" + searchText;
      if (priorSearch != null && priorSearch != searchText) {
        locationList = new ArrayList<>();
      }
    }
    priorSearch = searchText;

    ((MainActivity) getActivity()).getBeerFacade().getItems(url, BeerLocationList.class, new Callback<CollectionWrapper>() {
      @Override public void onTaskCompleted(CollectionWrapper result) {
        locationList.addAll(((BeerLocationList) result).getItems());
        nextUrl = result.getNext();
        locationListView.setIsBottomOfList(nextUrl == null);
        if (locationList.size() == 0) {
          emptyTextView.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
      }

      @Override public void onTaskFailure(String Reason) {

      }
    });
  }

  @Override public void onListScroll(int position, Callback<Object> callback) {
    callback.onTaskCompleted(null);
    if (!locationListView.getIsBottomOfList()) {
      fetchItems();
    }
  }

  public void setBeerId(String beerId) {
    this.beerId = beerId;
  }

  private class LocationListAdapter extends OnScrollListAdapter<BeerLocation> {
    LayoutInflater mInflater;

    public LocationListAdapter(Context context, int resource, OnScrollListener listener) {
      super(context, resource, locationList, listener);
      mInflater = LayoutInflater.from((context));
    }

    @Override public int getCount() {
      return locationList.size();
    }

    @Override public View getView(int i, View view, ViewGroup viewGroup) {
      if (view == null) {
        view = mInflater.inflate(R.layout.location_list_item_view, viewGroup, false);
      }
      checkScroll(i);


      TextView textView = ButterKnife.findById(view, R.id.location_list_item_name_view);
      textView.setText(locationList.get(i).getDisplayName());
      textView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());

      ImageView locationImageView = ButterKnife.findById(view, R.id.location_list_item_image);

      switch (locationList.get(i).getLicenseType()) {
        case CONSUME:
          locationImageView.setImageResource(R.drawable.location_type_restaurant);
          break;
        case RETAIL:
          locationImageView.setImageResource(R.drawable.location_type_grocery);
          break;
        case BREWERY:
          locationImageView.setImageResource(R.drawable.location_type_brewery);
          break;
        case GAS_STATION:
          locationImageView.setImageResource(R.drawable.location_type_gas_station);
          break;
        case UNKNOWN:
          locationImageView.setImageResource(R.drawable.location_type_unknown);
          break;
      }

      int rating = locationList.get(i).getRating();
      textView = ButterKnife.findById(view, R.id.location_list_item_rating_view);
      textView.setText(locationList.get(i).getDistance() + " miles");
      textView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());

      View imageView = ButterKnife.findById(view, R.id.location_list_item_num_stars_image_view);
      if (rating == 0) {
        imageView.setBackgroundResource(R.drawable.rating_0);
      } else if (rating == 1) {
        imageView.setBackgroundResource(R.drawable.rating_1);
      } else if (rating == 2) {
        imageView.setBackgroundResource(R.drawable.rating_2);
      } else if (rating == 3) {
        imageView.setBackgroundResource(R.drawable.rating_3);
      } else if (rating == 4) {
        imageView.setBackgroundResource(R.drawable.rating_4);
      } else if (rating == 5) {
        imageView.setBackgroundResource(R.drawable.rating_5);
      } else if (rating == 6) {
        imageView.setBackgroundResource(R.drawable.rating_6);
      }


      if (locationList.get(i).getSpecials().getItems().size() > 0) {
        view.setBackgroundColor(Color.parseColor("#DBC603"));
      } else {
        view.setBackgroundColor(Color.parseColor("#3B5A9c"));
      }
      return view;
    }
  }

}
