package com.beerbook.app.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectViews;
import butterknife.OnClick;
import com.beerbook.app.*;
import com.beerbook.app.R;
import com.beerbook.app.model.BeerLocation;
import com.beerbook.app.model.BeerLocationList;
import com.beerbook.app.model.CollectionWrapper;
import com.beerbook.app.rest.Callback;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public class BeerMapFragment extends Fragment implements OnMapReadyCallback {

  @InjectViews({R.id.beer_map_radius_text, R.id.beer_map_radius_toggle_5, R.id.beer_map_radius_toggle_10, R.id.beer_map_radius_toggle_20}) List<TextView>
      beerRadiusViews;
  GoogleMap mMap;
  ProgressDialog dialog;
  private Location myLocation;
  private LatLng storeLocation;
  private ArrayList<BeerLocation> locations = new ArrayList<>();


  @OnClick(R.id.beer_map_radius_toggle_5) void toggleFive(View button) {
    for (View view : beerRadiusViews) {
      view.setBackgroundColor(Color.parseColor("#899CC3"));
    }
    mMap.clear();
    fetchBeers(5, false);
    button.setBackgroundColor(Color.parseColor("#3B5A9c"));
  }
  @OnClick(R.id.beer_map_radius_toggle_10) void toggleTen(View button) {
    for (View view : beerRadiusViews) {
      view.setBackgroundColor(Color.parseColor("#899CC3"));
    }
    mMap.clear();
    fetchBeers(10, false);
    button.setBackgroundColor(Color.parseColor("#3B5A9c"));
  }
  @OnClick(R.id.beer_map_radius_toggle_20) void toggleTwenty(View button) {
    for (View view : beerRadiusViews) {
      view.setBackgroundColor(Color.parseColor("#899CC3"));
    }
    mMap.clear();
    fetchBeers(20, false);
    button.setBackgroundColor(Color.parseColor("#3B5A9c"));
  }


  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.activity_beer_map, container, false);

    ButterKnife.inject(this, root);
    SupportMapFragment mapFragment = (SupportMapFragment)
        getChildFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    for (TextView textView : beerRadiusViews) {
      textView.setTypeface(((MainActivity) getActivity()).getBeerTypeface());
    }

    return root;
  }

  //@Override public void onSaveInstanceState(Bundle outState) {
  //  super.onSaveInstanceState(outState);
  //  SupportMapFragment mapFragment = (SupportMapFragment)
  //      getChildFragmentManager().findFragmentById(R.id.map);
  //  FragmentTransaction ft = getChildFragmentManager().beginTransaction();
  //  ft.detach(mapFragment);
  //  ft.commit();
  //}
  //
  //@Override public void onDestroyView() {
  //  super.onDestroyView();
  //  try {
  //    SupportMapFragment mapFragment = (SupportMapFragment)
  //        getChildFragmentManager().findFragmentById(R.id.map);
  //    FragmentTransaction ft = getChildFragmentManager().beginTransaction();
  //    ft.detach(mapFragment);
  //    ft.commit();
  //  } catch (Exception e) {
  //
  //  }
  //
  //}

  private void fetchBeers(int radius, final boolean animate) {
    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    Criteria criteria = new Criteria();
    myLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
    if (myLocation != null) {
      if (((MainActivity) getActivity()).cameraPosition == null) {
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
            .target(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()))// Sets the center of the map to location user
            .zoom(8)                   // Sets the zoom
            .tilt(35)                   // Sets the tilt of the camera to 30 degrees
            .build()));
      } else {
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(((MainActivity) getActivity()).cameraPosition));
      }


      String url = "companies";
      url += "?latitude=" + myLocation.getLatitude();
      url += "&longitude=" + myLocation.getLongitude();
      url += "&radius=" + radius;
      fetchBeers(url, animate);

    } else {
      Toast.makeText(getActivity(), "Could not find your location!", Toast.LENGTH_SHORT).show();
      if (dialog != null) {
        dialog.dismiss();
      }
    }

  }


  private void fetchBeers(final String url, final boolean animate) {
    ((MainActivity) getActivity()).getBeerFacade().getItems(url, BeerLocationList.class, new Callback<CollectionWrapper>() {
      @Override public void onTaskCompleted(final CollectionWrapper result) {
        locations.addAll(((BeerLocationList) result).getItems());
        for (BeerLocation location : ((BeerLocationList) result).getItems()) {
          BitmapDescriptor bitmapDescriptor;
          switch (location.getLicenseType()) {
            case CONSUME:
              bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_restaurent_pin);
              break;
            case RETAIL:
              bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_grocery_pin);
              break;
            case BREWERY:
              bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_brewery_pin);
              break;
            case GAS_STATION:
              bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_gas_station_pin);
              break;
            case UNKNOWN:
            default:
              bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_unknown_pin);
              break;
          }

          mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
              .icon(bitmapDescriptor)
              .title(location.getId()));
        }

        if (result.getNext() != null) {
          fetchBeers(result.getNext().substring(1), false);
        }

        if (animate) {
          dialog.dismiss();
          LatLng position = storeLocation != null ? storeLocation : new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

          CameraPosition cameraPosition = new CameraPosition.Builder().target(position)      // Sets the center of the map to location user
              .zoom(15)                   // Sets the zoom
              .tilt(35)                   // Sets the tilt of the camera to 30 degrees
              .build();
          mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
      }

      @Override public void onTaskFailure(String Reason) {

      }
    });

  }

  @Override public void onPause() {
    super.onPause();
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  @Override
  public void onMapReady(GoogleMap map) {

    mMap = map;
    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    map.setMyLocationEnabled(true);
    if (dialog != null) {
      dialog.dismiss();
    }
    if (((MainActivity) getActivity()).isFirstLoad) {
      dialog = ProgressDialog.show(getActivity(), "Hold my beer..", "", true);
    }
    fetchBeers(5, ((MainActivity) getActivity()).isFirstLoad);
    ((MainActivity) getActivity()).isFirstLoad = false;

    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
      @Override public boolean onMarkerClick(Marker marker) {
        for (BeerLocation location : locations) {
          if (location.getId().equals(marker.getTitle())) {
            ((MainActivity) getActivity()).cameraPosition = mMap.getCameraPosition();
            ((MainActivity) getActivity()).viewingDetailsFromMap();
            BeerLocationDetailsFragment beerLocationDetailsFragment = new BeerLocationDetailsFragment();
            beerLocationDetailsFragment.beerLocation = location;
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_fragment_container, beerLocationDetailsFragment);
            ft.commit();
          }
        }
        return false;
      }
    });
  }
}