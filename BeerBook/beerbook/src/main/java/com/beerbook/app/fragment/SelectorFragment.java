package com.beerbook.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.beerbook.app.MainActivity;
import com.beerbook.app.R;

/**
 * Created by cameron.lewis on 11/27/14.
 */
public class SelectorFragment extends Fragment {

  @OnClick (R.id.selector_map_button) void onMapClicked() {
    BeerMapFragment beerMapFragment = new BeerMapFragment();
    beerMapFragment.setRetainInstance(true);
    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.main_fragment_container, beerMapFragment);
    ft.commit();
    ((MainActivity) getActivity()).onMapButtonClicked();
    ((MainActivity) getActivity()).showButtons();
  }

  @OnClick (R.id.selector_browser_button) void onBrowserClicked() {
    BeerFilterFragment beerFilterFragment = new BeerFilterFragment();
    beerFilterFragment.setRetainInstance(true);
    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.main_fragment_container, beerFilterFragment);
    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    ft.commit();
    ((MainActivity) getActivity()).showButtons();
    ((MainActivity) getActivity()).onBrowserButtonClicked();
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_selector, container, false);
    ButterKnife.inject(this, root);
    return root;
  }
}
