package com.beerbook.app.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.beerbook.app.R;
import java.lang.ref.SoftReference;

/**
 * Created by cameron.lewis on 8/13/14.
 */
public class SplashFragment extends DialogFragment {
  private View splashBackground;


  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(STYLE_NO_FRAME, 0);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.splash_fragment, container, false);
    ButterKnife.inject(this, rootView);

    splashBackground = ButterKnife.findById(rootView, R.id.splash_fragment_background);

    return rootView;
  }

  @Override public void onStart() {
    this.getDialog().getWindow().setLayout(-1, -1);
    this.setCancelable(false);

    super.onStart();
  }

  @Override public void onResume() {
    super.onResume();
    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.splash_fade_out);
    animation.setAnimationListener(new Animation.AnimationListener() {
      @Override public void onAnimationStart(Animation animation) {

      }

      @Override public void onAnimationEnd(Animation animation) {
        dismiss();
      }

      @Override public void onAnimationRepeat(Animation animation) {

      }
    });
    splashBackground.setAnimation(animation);
  }

}
