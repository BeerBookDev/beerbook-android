package com.beerbook.app.model;

import java.util.Collection;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class Beer extends DataObject {
  private String uid;
  private String displayName;
  private BeerBrand brand;
  private String description;
  private String abv;
  private BeerType type;
  private String brandName;

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public BeerBrand getBrand() {
    return brand;
  }

  public void setBrand(BeerBrand brand) {
    this.brand = brand;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAbv() {
    return abv;
  }

  public void setAbv(String abv) {
    this.abv = abv;
  }

  public BeerType getType() {
    return type;
  }

  public void setType(BeerType type) {
    this.type = type;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }
}
