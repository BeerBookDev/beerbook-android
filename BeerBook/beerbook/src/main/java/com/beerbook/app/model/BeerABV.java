package com.beerbook.app.model;

/**
 * Created by cameron.lewis on 11/22/15.
 */
public class BeerABV extends DataObject{

  private String value;
  private String displayName;

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
