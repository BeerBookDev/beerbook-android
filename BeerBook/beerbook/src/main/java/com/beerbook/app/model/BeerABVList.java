package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 11/22/15.
 */
public class BeerABVList extends CollectionWrapper {

  private ArrayList<BeerABV> items;

  public ArrayList<BeerABV> getItems() {
    return items;
  }

  public void setItems(ArrayList<BeerABV> items) {
    this.items = items;
  }
}