package com.beerbook.app.model;

/**
 * Created by cameron.lewis on 9/23/15.
 */
public class BeerBrand extends DataObject{

  private String brand;

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }
}
