package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 9/23/15.
 */
public class BeerBrandList extends CollectionWrapper {

  private ArrayList<BeerBrand> items;

  public ArrayList<BeerBrand> getItems() {
    return items;
  }

  public void setItems(ArrayList<BeerBrand> items) {
    this.items = items;
  }
}
