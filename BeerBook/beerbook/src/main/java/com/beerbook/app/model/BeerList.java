package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 8/30/15.
 */
public class BeerList extends CollectionWrapper {
  private ArrayList<Beer> items;

  public ArrayList<Beer> getItems() {
    return items;
  }

  public void setItems(ArrayList<Beer> items) {
    this.items = items;
  }
}
