package com.beerbook.app.model;

import java.util.Collection;

/**
 * Created by cameron.lewis on 6/30/15.
 */
public class BeerLocation extends DataObject {

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getAdminUrl() {
    return adminUrl;
  }

  public void setAdminUrl(String adminUrl) {
    this.adminUrl = adminUrl;
  }

  public float getDistance() {
    return distance;
  }

  public void setDistance(float distance) {
    this.distance = distance;
  }

  public enum LicenseType{
    BREWERY, GAS_STATION, CONSUME, RETAIL, UNKNOWN
  }

  private String uid;
  private LicenseType licenseType;
  private String legalName;
  private String displayName;
  private String street;
  private String street2;
  private String city;
  private String state;
  private String zip;
  private float latitude;
  private float longitude;
  private String address;
  private Integer rating;
  private String phone;
  private User owner;
  private boolean hasOwner;
  private SpecialList specials;
  private String website;
  private String adminUrl;
  private float distance;


  public BeerLocation() {

  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public LicenseType getLicenseType() {
    return licenseType;
  }

  public void setLicenseType(LicenseType licenseType) {
    this.licenseType = licenseType;
  }

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getStreet2() {
    return street2;
  }

  public void setStreet2(String street2) {
    this.street2 = street2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public boolean isHasOwner() {
    return hasOwner;
  }

  public void setHasOwner(boolean hasOwner) {
    this.hasOwner = hasOwner;
  }



  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public int getRating() {
    return rating;
  }

  public void setRating(int rating) {
    this.rating = rating;
  }


  public float getLatitude() {
    return latitude;
  }

  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }

  public float getLongitude() {
    return longitude;
  }

  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }

  public SpecialList getSpecials() {
    return specials;
  }

  public void setSpecials(SpecialList specials) {
    this.specials = specials;
  }
}
