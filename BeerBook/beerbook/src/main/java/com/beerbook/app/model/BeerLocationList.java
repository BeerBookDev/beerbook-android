package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 9/23/15.
 */
public class BeerLocationList extends CollectionWrapper {

  private ArrayList<BeerLocation> items;

  public ArrayList<BeerLocation> getItems() {
    return items;
  }

  public void setItems(ArrayList<BeerLocation> items) {
    this.items = items;
  }
}
