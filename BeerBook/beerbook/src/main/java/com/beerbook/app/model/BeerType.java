package com.beerbook.app.model;

import java.util.Collection;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class BeerType extends DataObject {

  private String type;
  private Collection<Beer> beers;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Collection<Beer> getBeers() {
    return beers;
  }

  public void setBeers(Collection<Beer> beers) {
    this.beers = beers;
  }
}
