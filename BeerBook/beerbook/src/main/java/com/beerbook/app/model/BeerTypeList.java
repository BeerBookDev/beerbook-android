package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 9/1/15.
 */
public class BeerTypeList extends CollectionWrapper {
  private ArrayList<BeerType> items;

  public ArrayList<BeerType> getItems() {
    return items;
  }

  public void setItems(ArrayList<BeerType> items) {
    this.items = items;
  }

}
