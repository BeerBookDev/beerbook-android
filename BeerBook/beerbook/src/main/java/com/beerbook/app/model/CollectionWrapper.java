package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class CollectionWrapper {
  private String next;
  private String previous;
  private String href;
  private Integer limit;
  private Integer offset;

  public CollectionWrapper() {
  }

  public String getNext() {
    return next;
  }

  public void setNext(String next) {
    this.next = next;
  }

  public String getPrevious() {
    return previous;
  }

  public void setPrevious(String previous) {
    this.previous = previous;
  }

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public Integer getOffset() {
    return offset;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

}
