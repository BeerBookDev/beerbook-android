package com.beerbook.app.model;

import java.util.Collection;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class Company extends DataObject {

  public Collection<Beer> getBeers() {
    return beers;
  }

  public void setBeers(Collection<Beer> beers) {
    this.beers = beers;
  }

  enum LicenseType{
    CONSUMER, RETAIL, UNKNOWN
  }

  private String uid;
  private LicenseType licenseType;
  private String legalName;
  private String displayName;
  private String street;
  private String street2;
  private String city;
  private String state;
  private String zip;
  private Double latitude;
  private Double longitude;
  private String address;
  private Integer rating;
  private String phone;
  private Collection<Beer> beers;
  private User owner;
  private boolean hasOwner;

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public LicenseType getLicenseType() {
    return licenseType;
  }

  public void setLicenseType(LicenseType licenseType) {
    this.licenseType = licenseType;
  }

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getStreet2() {
    return street2;
  }

  public void setStreet2(String street2) {
    this.street2 = street2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getRating() {
    return rating;
  }

  public void setRating(Integer rating) {
    this.rating = rating;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public boolean isHasOwner() {
    return hasOwner;
  }

  public void setHasOwner(boolean hasOwner) {
    this.hasOwner = hasOwner;
  }

}
