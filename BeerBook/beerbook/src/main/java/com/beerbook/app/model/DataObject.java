package com.beerbook.app.model;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class DataObject {
  private String id;
  private String href;

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
