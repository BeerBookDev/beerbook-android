package com.beerbook.app.model;

/**
 * Created by cameron.lewis on 11/22/15.
 */
public class Special extends DataObject {
  private String title;
  private String specialText;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSpecialText() {
    return specialText;
  }

  public void setSpecialText(String specialText) {
    this.specialText = specialText;
  }
}
