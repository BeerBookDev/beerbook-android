package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 11/22/15.
 */
public class SpecialList extends CollectionWrapper {
  private ArrayList<Special> items;

  public ArrayList<Special> getItems() {
    return items;
  }

  public void setItems(ArrayList<Special> items) {
    this.items = items;
  }
}
