package com.beerbook.app.model;

import java.util.ArrayList;

/**
 * Created by cameron.lewis on 9/1/15.
 */
public class StringList extends CollectionWrapper {
  private ArrayList<String> items;

  public ArrayList<String> getItems() {
    return items;
  }

  public void setItems(ArrayList<String> items) {
    this.items = items;
  }
}