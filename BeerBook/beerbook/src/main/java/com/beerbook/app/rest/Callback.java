package com.beerbook.app.rest;

/**
 * Created by cameron.lewis on 8/23/15.
 */
public abstract class Callback<T> {
  /**
   * Called when the HTTP request completes.
   *
   * @param result The result of the HTTP request.
   */
  public abstract void onTaskCompleted(T result);

  public abstract void onTaskFailure(String Reason);
}

