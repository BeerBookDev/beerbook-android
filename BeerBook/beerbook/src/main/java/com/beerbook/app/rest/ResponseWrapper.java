package com.beerbook.app.rest;

import java.util.Collection;
import java.util.LinkedHashMap;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class ResponseWrapper<T> {
  private Collection<String> errorBanners;
  private Collection<String> infoBanners;
  private Collection<String> successBanners;

  public T data;

  public Collection<String> getErrorBanners() {
    return errorBanners;
  }

  public void setErrorBanners(Collection<String> errorBanners) {
    this.errorBanners = errorBanners;
  }

  public Collection<String> getInfoBanners() {
    return infoBanners;
  }

  public void setInfoBanners(Collection<String> infoBanners) {
    this.infoBanners = infoBanners;
  }

  public Collection<String> getSuccessBanners() {
    return successBanners;
  }

  public void setSuccessBanners(Collection<String> successBanners) {
    this.successBanners = successBanners;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
