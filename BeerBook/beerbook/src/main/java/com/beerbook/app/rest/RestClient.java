package com.beerbook.app.rest;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import bolts.Capture;
import bolts.Continuation;
import bolts.Task;
import com.beerbook.app.util.Logger;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static java.util.Collections.singletonList;

/**
 * Created by cameron.lewis on 8/16/15.
 */
public class RestClient {

  Executor uiThreadExecutor;
  public String apiVersion;
  RestTemplate restTemplate;
  ObjectMapper objectMapper;
  private String HostIP = "http://beerbookapp.com/BeerBook/";
  private HashSet<Callback> callbacks;

  private String authToken= "token";
  public RestClient() {
    restTemplate = new RestTemplate();
    MappingJackson2HttpMessageConverter j = new MappingJackson2HttpMessageConverter();
    ByteArrayHttpMessageConverter b = new ByteArrayHttpMessageConverter();
    restTemplate.getMessageConverters().add(j);
    restTemplate.getMessageConverters().add(b);
    objectMapper = j.getObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


    callbacks = new HashSet<Callback>();
    uiThreadExecutor = new Executor() {
      public void execute(Runnable command) {
        new Handler(Looper.getMainLooper()).post(command);
      }
    };
    restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new ClientHttpRequestInterceptor() {
      @Override public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        HttpHeaders requestHeaders = new HttpHeaders();

        if (authToken != null) {
          requestHeaders.set("X-Auth-Token", authToken);
        }

        requestHeaders.set("Content-Type", "text=/json;charset=utf-8");
        requestHeaders.set("Accept", "text/json");
        requestHeaders.set("Accept-Language", "en;q=1, fr;q=0.9, de;q=0.8, zh-Hans;q=0.7, zh-Hant;q=0.6, ja;q=0.5");
        requestHeaders.set("User-Agent", "Android Tablet");

        Map<String, String> headersMap = requestHeaders.toSingleValueMap();
        httpRequest.getHeaders().setAll(headersMap);

        ClientHttpResponse response = execution.execute(new HttpRequestWrapper(httpRequest), body);
        Logger.debug(getClass().getSimpleName() + ">>> leaving intercept");

        return response;
      }
    }));
  }

  /**
   * Must be called after injection
   */
  public void setup() {

    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
    factory.setConnectTimeout(60 * 1000);
    factory.setReadTimeout(60 * 1000);
    restTemplate.setRequestFactory(factory);
  }

  public void cancelAllRequests() {
    if (callbacks != null) {
      callbacks.removeAll(callbacks);
    }
  }

  public void get(String path, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.GET, path, null, null, null, null, null, callback);
  }

  public void get(String path, Map<String, Object> params, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.GET, path, null, params, clazz, null, null, callback);
  }

  public void get(String path, Map<String, Object> params, Class clazz, Class nestedClazz, String nestedKey, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.GET, path, null, params, clazz, nestedClazz, nestedKey, callback);
  }

  public void post(String path, Object params, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.POST, path, params, null, clazz, null, null, callback);
  }

  public void post(String path, Map<String, Object> params, Class clazz, Class nestedClazz, String nestedKey, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.POST, path, params, null, clazz, nestedClazz, nestedKey, callback);
  }

  public void post(String path, Object params, Map<String, Object> urlParams, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.POST, path, params, urlParams, clazz, null, null, callback);
  }

  public void put(String path, Object params, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.PUT, path, params, null, clazz, null, null, callback);
  }

  public void put(String path, Object params, Map<String, Object> urlParams, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.PUT, path, params, urlParams, clazz, null, null, callback);
  }

  public void delete(String path, Object params, Class clazz, Callback callback) {
    httpRequest(org.springframework.http.HttpMethod.DELETE, path, params, null, clazz, null, null, callback);
  }


  public void httpRequest(final org.springframework.http.HttpMethod httpMethod, final String url, final Object params, final Map<String, Object> urlParams, final Class clazz,
      final Class nestedClazz, final String nestedKey, final Callback callback) {
    final Capture<RestTemplate> sharedRestTemplate = new Capture<RestTemplate>(restTemplate);
    final Capture<ObjectMapper> sharedObjectMapper = new Capture<ObjectMapper>(objectMapper);
    final Capture<HashSet<Callback>> sharedCallbacks = new Capture<HashSet<Callback>>(callbacks);


    callbacks.add(callback);



    Callable<ResponseWrapper> callable = new Callable<ResponseWrapper>() {
      @Override public ResponseWrapper call() throws Exception {
        ResponseWrapper responseWrapper = null;

        try {
          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HostIP + url);

          URI uri;
          if ((urlParams != null) && (urlParams.size() != 0)) {
            for (String key : urlParams.keySet()) {
              builder.queryParam(key, urlParams.get(key));
            }
          }

          uri = builder.build().encode().toUri();

          switch (httpMethod) {

            case GET:
              responseWrapper = sharedRestTemplate.get().getForObject(uri, ResponseWrapper.class);
              break;
            case POST:
              responseWrapper = sharedRestTemplate.get().postForObject(uri, params, ResponseWrapper.class);
              break;
            case HEAD:
              break;
            case OPTIONS:
              break;
            case PUT:
              HttpEntity<Object> httpEntityPut = new HttpEntity<Object>(params, new HttpHeaders());

              responseWrapper = sharedRestTemplate.get().exchange(uri, org.springframework.http.HttpMethod.PUT, httpEntityPut, ResponseWrapper.class).getBody();

              break;
            case DELETE:
              HttpEntity<ResponseWrapper> httpEntityDelete = new HttpEntity<ResponseWrapper>(new HttpHeaders());
              responseWrapper =
                  sharedRestTemplate.get().exchange(url, org.springframework.http.HttpMethod.DELETE, httpEntityDelete, ResponseWrapper.class, params).getBody();
              break;
            case TRACE:
              break;
            default:
              responseWrapper = sharedRestTemplate.get().getForObject(uri, ResponseWrapper.class);

              break;
          }
        } catch (Exception e) {
          String message = e.getMessage() != null ? e.getMessage() : "Unknown Error";
          Logger.warn(httpMethod + " Request Exception: " + message);
          ResponseWrapper rw = new ResponseWrapper();

          return null;
        }

        if (!sharedCallbacks.get().contains(callback)) {
          return null;
        }

        if (responseWrapper == null) {
          responseWrapper = new ResponseWrapper();
          return responseWrapper;
        }

        if (clazz != null) {
          if (clazz == Void.class) {
            //                        responseWrapper.data = null;
          }

          Object data = sharedObjectMapper.get().convertValue(responseWrapper.data, clazz);
          if (nestedClazz != null && nestedKey != null) {
            Object nestedData = sharedObjectMapper.get().convertValue(((LinkedHashMap) responseWrapper.data).get(nestedKey), nestedClazz);
            try {
              String nestedSetter = "set" + nestedKey.substring(0, 1).toUpperCase() + nestedKey.substring(1, nestedKey.length());
              Class mparams[] = new Class[1];
              mparams[0] = nestedClazz.getSuperclass();
              Method method = clazz.getDeclaredMethod(nestedSetter, mparams);
              try {
                method.invoke(data, nestedData);
              } catch (IllegalAccessException e) {
                Logger.printStackTrace(e);
              } catch (InvocationTargetException e) {
                Logger.printStackTrace(e);
              }
            } catch (NoSuchMethodException e) {
              Logger.printStackTrace(e);
            }
          }
          responseWrapper.data = data;
        }
        return responseWrapper;
      }
    };

    final Continuation<ResponseWrapper, Void> continuation = new Continuation<ResponseWrapper, Void>() {

      public Void then(Task<ResponseWrapper> responseWrapperTask) throws Exception {
        if (!sharedCallbacks.get().contains(callback)) {
          return null;
        } else {
          sharedCallbacks.get().remove(callback);

          if (responseWrapperTask.isFaulted()) {
            Logger.printStackTrace(responseWrapperTask.getError());
            throw responseWrapperTask.getError();
          }

          ResponseWrapper responseWrapper = responseWrapperTask.getResult();
          if (responseWrapper != null) {
            try {
              if (clazz != null) {
                callback.onTaskCompleted(responseWrapper.data);
              } else {
              }
            } catch (Exception e) {
              Logger.printStackTrace(e);
            }

          } else {

            Logger.warn("Failure");
            if (responseWrapper != null) {
            } else {
              callback.onTaskFailure("Failed to reach server");
            }
          }

          return null;
        }
      }
    };

    try {
      Task.callInBackground(callable).continueWith(continuation, uiThreadExecutor);
    } catch (Exception e) {
      Logger.printStackTrace(e);
      callback.onTaskFailure(e.getMessage());
    }
  }

  public byte[] checkRequest(String path) {
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(singletonList(MediaType.TEXT_HTML));

      path = path + "?format=" + MediaType.TEXT_HTML_VALUE;

      HttpEntity<byte[]> entity = restTemplate.exchange(HostIP + path, org.springframework.http.HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
      return entity.getBody();
    } catch (Exception e) {
      Logger.error("Image Request Failure", e);
      Logger.printStackTrace(e);
      return null;
    }
  }

  public byte[] fileRequest(String path) {
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(singletonList(MediaType.ALL));

      HttpEntity<byte[]> entity = restTemplate.exchange(HostIP + path, org.springframework.http.HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
      return entity.getBody();
    } catch (Exception e) {
      Logger.error("Image Request Failure", e);
      Logger.printStackTrace(e);
      return null;
    }
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public String getAuthToken() {
    return authToken;
  }

  class AcceptHeaderHttpRequestInterceptor implements ClientHttpRequestInterceptor {
    private final String headerValue;

    public AcceptHeaderHttpRequestInterceptor(String headerValue) {
      this.headerValue = headerValue;
    }

    @Override public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

      HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
      requestWrapper.getHeaders().setAccept(singletonList(MediaType.ALL));

      return execution.execute(requestWrapper, body);
    }
  }

  public <T> ResponseWrapper mockCall(Class<T> clazz, HttpMethod httpMethod, String path) throws IOException {
     return null;
  }

  public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
  }

  public class PutHttpTask extends AsyncTask<Void, Void, ResponseWrapper> {

    private Callback _callback;
    private String _path;
    private RestTemplate _restTemplate;
    private Object _params;
    private Class _clazz;

    public PutHttpTask(String path, Object params, RestTemplate restTemplate, Class clazz, Callback callback) {
      this._callback = callback;
      this._path = path;
      this._restTemplate = restTemplate;
      this._params = params;
      this._clazz = clazz;
    }

    @Override protected ResponseWrapper doInBackground(Void... params) {
      final String url;
      try {
        if (_path.equalsIgnoreCase("info")) {
          url = HostIP + "/api/" + _path;
        } else if (_path.contains("api/")) {
          url = HostIP + _path;
        } else {
          url = HostIP + "/api/" + apiVersion + "/" + _path;
        }

        HttpEntity<ResponseWrapper> httpEntity = new HttpEntity<ResponseWrapper>(new HttpHeaders());
        ResponseEntity<ResponseWrapper> responseEntity =
            restTemplate.exchange(url, org.springframework.http.HttpMethod.PUT, httpEntity, ResponseWrapper.class, _params);
        return responseEntity.getBody();
      } catch (Exception e) {
        Logger.warn("PUT Request Exception:" + e.getMessage());
        _callback.onTaskFailure("Exception" + e.getMessage());
      }

      return null;
    }

    @Override protected void onPostExecute(ResponseWrapper responseWrapper) {
      if (responseWrapper != null) {
        Object data = objectMapper.convertValue(responseWrapper, _clazz);
        _callback.onTaskCompleted(data);
      } else {
        Logger.warn("Failure");
        _callback.onTaskFailure("failure in onPostExecute");
      }
    }
  }

  public class DeleteHttpTask extends AsyncTask<Void, Void, ResponseWrapper> {

    private Callback _callback;
    private String _path;
    private RestTemplate _restTemplate;
    private Object _params;
    private Class _clazz;

    public DeleteHttpTask(String path, Object params, RestTemplate restTemplate, Class clazz, Callback callback) {
      this._callback = callback;
      this._path = path;
      this._restTemplate = restTemplate;
      this._params = params;
      this._clazz = clazz;
    }

    @Override protected ResponseWrapper doInBackground(Void... params) {
      try {
        final String url = HostIP + "/api/" + apiVersion + "/" + _path;
        HttpEntity<ResponseWrapper> httpEntity = new HttpEntity<ResponseWrapper>(new HttpHeaders());
        ResponseEntity<ResponseWrapper> responseEntity =
            restTemplate.exchange(url, org.springframework.http.HttpMethod.DELETE, httpEntity, ResponseWrapper.class, _params);
        return responseEntity.getBody();
      } catch (Exception e) {
        Logger.warn("DELETE Request Exception:" + e.getMessage());
        //                _callback.onTaskFailure("Exception" + e.getMessage());

      }

      return null;
    }

    @Override protected void onPostExecute(ResponseWrapper responseWrapper) {
      if (responseWrapper != null) {

        Object data = objectMapper.convertValue(responseWrapper, _clazz);
        _callback.onTaskCompleted(data);
      } else {
        Logger.warn("Failure");
        _callback.onTaskFailure("failure in onPostExecute");
      }
    }
  }
}
