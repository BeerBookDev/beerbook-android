package com.beerbook.app.util;

import java.util.TreeSet;
import java.util.Set;

/**
 * Created by cameron.lewis on 6/15/15.
 */
public class BeerUtil {
  public static TreeSet<String> beerBrandSet = new TreeSet();
  public static TreeSet<String> beerNameSet = new TreeSet();
  public static TreeSet<String> beerStyleSet = new TreeSet();

  public static void constructSets() {
    for (String string : BeerBrandUtil.brandList) {
      beerBrandSet.add(string);
    }

    for (String string : BeerNameUtil.beerList) {
      beerNameSet.add(string);
    }

    for (String string : BeerStyleUtil.styleList) {
      beerStyleSet.add(string);
    }
  }

}
