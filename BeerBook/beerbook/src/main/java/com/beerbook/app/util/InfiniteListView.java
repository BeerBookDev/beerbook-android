package com.beerbook.app.util;

/**
 * Created by cameron.lewis on 9/23/15.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.beerbook.app.R;

public class InfiniteListView extends ListView {
  private LinearLayout footerContainer;
  private RelativeLayout footer;
  private ProgressBar footerSpinner;
  private boolean mBottomOfList;

  public InfiniteListView(Context context) {
    super(context);
    init();
  }

  public InfiniteListView(android.content.Context context, android.util.AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public InfiniteListView(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }


  private void init() {
    super.onFinishInflate();
    footerContainer =
        (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.ptr_footer, null);

    footer = (RelativeLayout) footerContainer.findViewById(R.id.ptr_id_footer);
    footer.setVisibility(INVISIBLE);

    footerSpinner = (ProgressBar) footer.findViewById(R.id.ptr_id_spinner_footer);
    footerSpinner.setVisibility(INVISIBLE);

    addFooterView(footerContainer);
    mBottomOfList = false;
  }

  public void animateFooterSpinner() {
    footer.setVisibility(VISIBLE);
    footerSpinner.setVisibility(VISIBLE);
    try {
      footerSpinner.animate();
    } catch (Exception e) {
      Log.d("debug", "literally whatever");
    }
  }

  public void finishAnimateFooterSpinner() {
    footerSpinner.clearAnimation();
    footer.setVisibility(INVISIBLE);
    footerSpinner.setVisibility(INVISIBLE);
  }

  public boolean getIsBottomOfList() {
    return mBottomOfList;
  }

  public void setIsBottomOfList(boolean bottom) {
    mBottomOfList = bottom;
  }

  public void removeFooter() {
    removeFooterView(footerContainer);
    mBottomOfList = true;
  }
}
