package com.beerbook.app.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

/**
 * Created by cameron.lewis on 11/6/15.
 */
public class LocationUtility {

  public static Location getLocation(Context context) {
    LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    Criteria criteria = new Criteria();
    return locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
  }
}
