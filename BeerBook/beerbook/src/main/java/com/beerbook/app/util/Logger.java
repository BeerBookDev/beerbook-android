package com.beerbook.app.util;

/**
 * Created by cameron.lewis on 8/23/15.
 */

import android.util.Log;

public class Logger {
  public static final String DEBUG_TAG = "BeerBookDebug"; //filter logcat with this tag

  public static void verbose(String verbose) {
    Log.v(DEBUG_TAG, verbose);
  }

  public static void debug(String debugText) {
    Log.d(DEBUG_TAG, debugText);
  }

  public static void warn(String warnText) {
    Log.w(DEBUG_TAG, warnText);
  }

  public static void error(String warnText) {
    Log.e(DEBUG_TAG, warnText);
  }

  public static void error(String warnText, Throwable throwable) {
    Log.e(DEBUG_TAG, warnText, throwable);
  }

  public static void printStackTrace(Throwable throwable) {
    throwable.printStackTrace();
  }
}
