package com.beerbook.app.util;

/**
 * Created by cameron.lewis on 9/23/15.
 */

import android.content.Context;
import android.widget.ArrayAdapter;
import com.beerbook.app.rest.Callback;
import java.util.List;

public class OnScrollListAdapter<T> extends ArrayAdapter<T> {

  protected OnScrollListener scrollListener;
  private int currentPage = 0;
  protected boolean enabled = true;
  public boolean filterLocked = false;

  public void reset() {
    this.enabled = true;
    this.currentPage = 0;
  }

  public OnScrollListAdapter(Context context, int textViewResourceId, List<T> items) {
    super(context, textViewResourceId, items);
  }

  public OnScrollListAdapter(Context context, int textViewResourceId, List<T> items, OnScrollListener listener) {
    super(context, textViewResourceId, items);
    scrollListener = listener;
  }

  protected void checkScroll(int position) {
    //Check if we're at the end of the list
    if( enabled && !filterLocked && position == getCount() - 1 && scrollListener != null) {
      currentPage++;
      enabled = false;
      scrollListener.onListScroll(currentPage, new Callback<Object>() {
        @Override
        public void onTaskCompleted(Object result) {
          enabled = true;
        }

        @Override
        public void onTaskFailure(String Reason) {
          enabled = true;
        }
      });
    }
  }

  public interface OnScrollListener {
    public void onListScroll(int position,Callback<Object> callback);
  }

}